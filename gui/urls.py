from django.urls import path

from . import views

app_name = 'gui'
urlpatterns = [
    path('', views.index, name='index'),
]