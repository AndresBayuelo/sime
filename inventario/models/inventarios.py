from django.db import models
from .productos import *

class Inventario(models.Model):
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
    valor = models.DecimalField(max_digits=12, decimal_places=2)
    class Meta:
        abstract=True

class InventarioInsumo(Inventario):
    insumo = models.OneToOneField(Insumo, on_delete=models.PROTECT)
    class Meta(Inventario.Meta):
        db_table = 'inventario_insumo'

class InventarioServicio(Inventario):
    servicio = models.OneToOneField(Servicio, on_delete=models.PROTECT)
    class Meta(Inventario.Meta):
        db_table = 'inventario_servicio'

class InventarioProductoFabricado(Inventario):
    productofabricado = models.OneToOneField(ProductoFabricado, on_delete=models.PROTECT)
    class Meta(Inventario.Meta):
        db_table = 'inventario_productofabricado'