from django.db import models

class Producto(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    unidadmedida = models.CharField(max_length=20)
    class Meta:
        abstract=True

class Insumo(Producto):
    class Meta(Producto.Meta):
        db_table = 'insumo'

class Servicio(Producto):
    class Meta(Producto.Meta):
        db_table = 'servicio'

class ProductoFabricado(Producto):
    insumos = models.ManyToManyField(Insumo,through='InsumoProducto')
    servicios = models.ManyToManyField(Servicio,through='ServicioProducto')
    class Meta(Producto.Meta):
        db_table = 'productofabricado'

class InsumoProducto(models.Model):
    productofabricado = models.ForeignKey(ProductoFabricado, on_delete=models.PROTECT)
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
    cantidad_desperdicio = models.DecimalField(max_digits=10, decimal_places=2)
    class Meta:
        db_table = 'insumo_producto'

class ServicioProducto(models.Model):
    productofabricado = models.ForeignKey(ProductoFabricado, on_delete=models.PROTECT)
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
    class Meta:
        db_table = 'servicio_producto'

class ProcesamietoProducto(models.Model):
    fecha = models.DateField()
    productofabricado = models.ForeignKey(ProductoFabricado, on_delete=models.PROTECT)
    cantidad = models.IntegerField(default=1)
    porcentaje_utilidad = models.IntegerField(default=0)