from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Insumo)
admin.site.register(Servicio)
admin.site.register(ProductoFabricado)
admin.site.register(InsumoProducto)
admin.site.register(ServicioProducto)
admin.site.register(ProcesamietoProducto)
admin.site.register(InventarioInsumo)
admin.site.register(InventarioServicio)
admin.site.register(InventarioProductoFabricado)